## Introduction
Thank you for the opportunity for me to doing the catch code challenge.
## Installation
After clone the repository, install the npm by running the script below:
- npm install
## Running the development server
After installing the npm, try to run the npm by running the script below: 
- npm start

## Architecture
1. App.js
This js file will contains Container (from react-bootstrap), ProductSort component, Products (if products length is more than 0). The system will show Loading text while we fetch the data and if there's no products it will show "Found no products.".
2. UI/Card
Card template for Product Card.
3. Products
- ProductItem
This js file will show product column and product details. 
- ProductHeading
This js file will show the meta data.
- ProductsSort
This js file will show the Catch's logo and filter dropdown