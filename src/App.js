import React, { Fragment,useState,useEffect } from 'react';
import './App.css';
import ProductsSort from './components/Products/ProductsSort';
import Products from './components/Products/Products';
import Container from 'react-bootstrap/Container';

function App() {
  const [products, setProducts] = useState([]);
  const [metaData, setMetaData] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  const [sortType, setSortType] = useState('high');
  let sortedProducts = products.sort((a, b) => parseFloat(a.salePrice) - parseFloat(b.salePrice));
  const sortChangeHandler = selectedSortType => {
    setSortType(selectedSortType);
  };
  
  if(sortType === 'high'){
    sortedProducts = products.sort((a, b) => parseFloat(b.salePrice) - parseFloat(a.salePrice));
  }else{
    sortedProducts = products.sort((a, b) => parseFloat(a.salePrice) - parseFloat(b.salePrice));
  }
  
  useEffect(() => {
    const fetchProducts = () => {
        setIsLoading(true);
        fetch('http://catch-code-challenge.s3-website-ap-southeast-2.amazonaws.com/challenge-3/response.json')
        .then((response) => response.json())
        .then((res) => {
          const getData = res.results.map((productData) => {
            return {
              id: productData.id,
              name: productData.name,
              salePrice: productData.salePrice,
              retailPrice: productData.retailPrice,
              imageUrl: productData.imageUrl,
              quantityAvailable: productData.quantityAvailable,
            };
          });
          setProducts(getData);
          setIsLoading(false);
          setMetaData(res.metadata);
        });
     };
     fetchProducts();
  }, []);

  return (
    <Fragment>
      <Container>
        <ProductsSort metadata={metaData} isLoading={ isLoading } onChangeSort={sortChangeHandler} selected={sortType}></ProductsSort>
        {!isLoading && products.length > 0 && <Products products={sortedProducts} />}
        {!isLoading && products.length === 0 && <p>Found no products.</p>}
        {isLoading && <p>Loading...</p>}
      </Container>
    </Fragment>
  );
}

export default App;
