import { render, screen } from '@testing-library/react';
import App from './App';

describe('App component', () => {
    test('renders products if request succeds', async () => {
        window.axios = jest.fn();
        window.axios.mockResolvedValueOnce({
            json: async() => [{ 
                metadata: { query: "best sellers", total: "100",page: 1, pages: 3}, 
                results : { id: "p1",name:"Product 1", salePrice: 1000,retailPrice: 5000,imageUrl:"https://s.catch.com.au/images/product/0002/2114/593f690189ac9183721654_w200.jpg",quantityAvailable: 0}}],
        });
        render(<App/>);
        const listProducts = await screen.findAllByRole('product-list');
        expect(listProducts).not.toHaveLength(0);
    });

});