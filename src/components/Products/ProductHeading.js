import React from 'react';
import classes from './ProductHeading.module.css';

const ProductHeading = props => {
    return <div className={classes['products-heading']}>
        <span> Showing { props.metadata.total } products. </span>
        <span>{ props.metadata.page } of { props.metadata.pages } pages.</span>
    </div>;
};

export default ProductHeading;