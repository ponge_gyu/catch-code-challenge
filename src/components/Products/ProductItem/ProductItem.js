import React from 'react';
import Col from 'react-bootstrap/Col';
import classes from './ProductItem.module.css';
import Card from '../../UI/Card';

const ProductItem = props => {
    const product = props.product;
    const retailPrice = product.retailPrice * 0.01;
    const fixedRetailPrice = `$${retailPrice.toFixed(2)}`;
    const salePrice = product.salePrice * 0.01;
    const fixedSalePrice = `$${salePrice.toFixed(2)}`;
    return <Col className={classes['col-product']} lg={3} xs={6} role="product-list">
        <Card>
            <div className={classes['product-image']}>
                <img src={product.imageUrl} alt={product.name}/>
                {product.quantityAvailable <= 0 && <div className={classes['sold-out-badge']}>Sold Out</div>}
            </div>
            <div className={classes['product-name']}>{product.name}</div>
            {retailPrice > 0 && <div className={classes['product-retail']}>{fixedRetailPrice}</div>}
            
            <div className={`${classes['product-sale']} ${retailPrice <= 0 ? classes['without-retail-price'] : ''}`}>{fixedSalePrice}</div>
        </Card>
    </Col>
};

export default ProductItem;