import React from 'react';
import classes from './Products.module.css';
import Row from 'react-bootstrap/Row';
import ProductItem from './ProductItem/ProductItem';

const Products = props => {

    const productsList = props.products.map(product => <ProductItem id={product.id} key={product.id} product={product}/>);
    return <section className={classes.products}>
        <Row>
            {productsList}
        </Row>
    </section>;
};

export default Products;