import React,{ Fragment } from 'react';
import logoImage from '../../assets/logo.svg';
import classes from './ProductsSort.module.css';
import ProductHeading from './ProductHeading';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const ProductsSort = props => {
    
    const sortChangeHandler = (event) => {
        props.onChangeSort(event.target.value);
    }
    return (
    <Fragment>
    <div className={classes['products-sort']}>
        <Row>
            <Col lg={6}>
                <div className={classes.logo}>
                    <img src={logoImage} alt='Catch.com.au'/>
                </div>
                {!props.isLoading && <ProductHeading metadata={ props.metadata }/>}
            </Col>
            <Col lg={6} className={classes['col-sort']}>
                <div>
                    <select onChange={sortChangeHandler} value={props.selected}>
                        <option value='high'>Price (High - Low)</option>
                        <option value='low'>Price (Low - High)</option>
                    </select>
                </div>
            </Col>
        </Row>
    </div>
    </Fragment>);
};

export default ProductsSort;